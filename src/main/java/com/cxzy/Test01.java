package com.cxzy;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;


public class Test01 extends Application {

    /*public Test01() throws InterruptedException {
        // 谷歌内核浏览器
        Browser browser = new Browser();
        BrowserView view = new BrowserView(browser);
        //加载地址
        browser.loadURL("https://passport.bilibili.com/login");
        this.add(view);
        this.setTitle("哩啵哩啵在线多人视频互助系统");
        this.setBounds(300, 200, 500, 600);
        this.setSize(1280, 760);
        this.setLocation(400, 200);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //关闭按钮功能
        this.setVisible(true);//设置窗体可见
        this.setResizable(false); //禁止改变窗口大小

        Container c = this.getContentPane(); //获取窗体容器

        c.setBackground(Color.gray); //设置背景颜色


        //是否在屏幕最上层显示
        this.setAlwaysOnTop(true);

        String bili_jct = ""; //用户登录cookie

        cc:
        while (true) {
            List<Cookie> allCookies = browser.getCookieStorage().getAllCookies();
            for (Cookie allCookie : allCookies) {
                if (allCookie.getName().equals("bili_jct")) {
                    bili_jct = allCookie.getValue();
                    break cc;
                }
            }
            System.out.println("jiankong");
            Thread.sleep(1000);
        }

        System.out.println("用户cookie获取成功!是:"+bili_jct);

        this.setContentPane(c);

    }*/

    @Override
    public void start(Stage primaryStage) {
        final AnchorPane root = new AnchorPane();
        Scene scene = new Scene(root);

        Button button = new Button("", null);
        button.setLayoutX(0);
        button.setLayoutY(0);

        button.setPrefSize(100, 100); //设置按钮宽和高
        button.setId("loginButton");
        button.setStyle("-fx-background-image:url('https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3352424404,177234181&fm=26&gp=0.jpg');" +
                "-fx-background-size: 100%;" +
                "-fx-background-repeat: no-repeat;" +
                "-fx-background-radius: 5;");

        //设置按钮功能
        button.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                startWebView(root);
            }
        });

        MenuBar menuBar = new MenuBar();

        root.getChildren().add(button);
        root.getChildren().add(setMenu(menuBar));


        primaryStage.setScene(scene);
        primaryStage.setHeight(720);
        primaryStage.setWidth(1280);
        primaryStage.setAlwaysOnTop(false);//是否在屏幕最上层显示
        primaryStage.setTitle("哔狸哔狸互助系统");

        primaryStage.show();
    }

    /**
     * 创建webview登录b站页面获取cookie
     *
     * @param root
     */
    public static String startWebView(AnchorPane root) {
        final WebView webView = new WebView();
        WebEngine engine = webView.getEngine();
        engine.load("https://passport.bilibili.com/login");
        //禁止右键
        webView.setContextMenuEnabled(false);

        /**
         * 绑定窗口高度宽度
         */
        webView.prefHeightProperty().bind(root.widthProperty());
        webView.prefWidthProperty().bind(root.widthProperty());
        root.getChildren().add(webView);
        return "";
    }

    public static void main(String[] args) throws InterruptedException {
        launch(args);
    }

    public static MenuBar setMenu(MenuBar menuBar) {
        Menu menu1 = new Menu("菜单1");
        Menu menu2 = new Menu("菜单2");

        MenuItem menuItem1 = new MenuItem("字菜单1");
        MenuItem menuItem2 = new MenuItem("字菜单2");

        menu1.getItems().addAll(menuItem1,menuItem2);
        menu2.getItems().addAll(menuItem1,menuItem2);

        menuBar.getMenus().addAll(menu1, menu2);
        return menuBar;
    }

}
